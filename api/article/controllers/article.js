const { sanitizeEntity } = require("strapi-utils");

module.exports = {
  /**
   * Retrieve a record.
   *
   * @return {Object}
   */

  async findOne(ctx) {
    const { slug } = ctx.params;

    const entity = await strapi.services.article.findOne({ slug });
    return sanitizeEntity(entity, { model: strapi.models.article });
  },

  async findSlugs(ctx) {
    const entity = await strapi.query("article").model.find().select(["slug"]);

    return sanitizeEntity(entity, {
      model: strapi.models.article,
      includeFields: ["id", "slug"],
    });
  },
};
