module.exports = {
  settings: {
    gzip: {
      enabled: true,
      options: {
        br: false,
      },
    },
  },
  public: {
    maxAge: 1000 * 3600 * 24 * 30, // cache 30days
    path: "./public",
  },
};
